<?php
App::uses('AppController', 'Controller');
/**
 * Schedulesdetails Controller
 *
 * @property Schedulesdetail $Schedulesdetail
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class SchedulesdetailsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'RequestHandler');
	var $uses = array('Schedule', 'Airline', 'Driver', 'Vehicle', 'Location', 'Schedulesdetail');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Schedulesdetail->recursive = 0;
		$this->set('schedulesdetails', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Schedulesdetail->exists($id)) {
			throw new NotFoundException(__('Invalid schedulesdetail'));
		}
		$options = array('conditions' => array('Schedulesdetail.' . $this->Schedulesdetail->primaryKey => $id));
		$this->set('schedulesdetail', $this->Schedulesdetail->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add($id = null) {

		if ($this->request->is('post')) {
			$ds['Schedulesdetail']= array(
				'schedule_id' => $this->request->data['Schedulesdetail']['schedule_id'],
				'location_id' => $this->request->data['Schedulesdetail']['location_id'],				
				'crew' => $this->request->data['Schedulesdetail']['crew'],
				'flight_state' => $this->request->data['Schedulesdetail']['flight_state'],
				'pickup_crew' => $this->request->data['Schedulesdetail']['pickup_crew'],
				'drop_crew' => $this->request->data['Schedulesdetail']['drop_crew'],
				'driver_id' => $this->request->data['Schedulesdetail']['driver_id'],
				'vehicle_id' => $this->request->data['Schedulesdetail']['vehicle_id'],
				'comments' => $this->request->data['Schedulesdetail']['comments'],
				'estimated_time' => $this->request->data['Schedulesdetail']['estimated_time'],
				'date' => date("Y-m-d", strtotime($this->request->data['Schedulesdetail']['date']))
				);

			if ($this->Schedulesdetail->save($ds)) {
				$this->Session->setFlash(__('Schedule row added.'), 'flash_good');
				return $this->redirect(array('controller'=>'Schedules', 'action' => 'view', $this->request->data['Schedulesdetail']['schedule_id']));
			} else {
				$this->Session->setFlash(__('The schedule row could not be saved. Please, try again.'), 'flash_bad');
			}
		}

		$locations = $this->Location->find('list');
		$drivers = $this->Driver->find('list');
		$vehicles = $this->Vehicle->find('list');
		$this->set(compact('locations', 'drivers', 'vehicles','id'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id_schedulesdetail = null, $id_schedule = null) {		
		if (!$this->Schedulesdetail->exists($id_schedulesdetail)) {
			throw new NotFoundException(__('Invalid schedulesdetail'));
		}
		if ($this->request->is(array('post', 'put'))) {
			 $this->request->data['Schedulesdetail']['date'] = date("Y-m-d", strtotime($this->request->data['Schedulesdetail']['date']));
			if ($this->Schedulesdetail->save($this->request->data)) {
				$this->Session->setFlash(__('The schedule detail has been updated.'), 'flash_good');
				return  $this->redirect($this->request->data['Referer']['referer'] );
			} else {
				$this->Session->setFlash(__('The schedule detail could not be updated. Please, try again.'), 'flash_bad');
			}
		} else {
			$options = array('conditions' => array('Schedulesdetail.' . $this->Schedulesdetail->primaryKey => $id_schedulesdetail));
			$this->request->data = $this->Schedulesdetail->find('first', $options);
		}
		$referer = $this->referer();
		$schedules = $this->Schedulesdetail->Schedule->find('list');
		$locations = $this->Schedulesdetail->Location->find('list');
		$drivers = $this->Schedulesdetail->Driver->find('list');
		$vehicles = $this->Schedulesdetail->Vehicle->find('list');
		$this->set(compact('schedules', 'locations', 'drivers', 'vehicles', 'id_schedulesdetail', 'id_schedule', 'referer'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null, $schedule_id = null) {
		$this->Schedulesdetail->id = $id;
		if (!$this->Schedulesdetail->exists()) {
			throw new NotFoundException(__('Invalid schedulesdetail'));
		}
		//$this->request->allowMethod('post', 'delete');
		if ($this->Schedulesdetail->delete()) {
			$this->Session->setFlash(__('The row has been deleted.'), 'flash_good');			
		} else {
			$this->Session->setFlash(__('The row could not be deleted. Please, try again.'), 'flash_bad');			
		}
		return $this->redirect(array('controller'=>'Schedules', 'action' => 'view', $schedule_id));
	}

	public function agenda(){
		if ($this->request->is(array('post', 'put'))) {

			$ds_dates = explode(',', $this->request->data['Schedulesdetail']['date']);

			foreach ($ds_dates as $ds_date) {
				$date[] = date("Y-m-d", strtotime($ds_date));
			}

		  	if($this->request->data['Schedulesdetail']['flight_type'] == 'All'){
		  		if(empty($this->request->data['Schedulesdetail']['flight_number']) && !empty($this->request->data['Schedulesdetail']['date'])){		  			
		  			$conditions = array('Schedulesdetail.date' => $date);
		  		}
		  		elseif(!empty($this->request->data['Schedulesdetail']['flight_number']) && empty($this->request->data['Schedulesdetail']['date'])){
		  			$conditions = array('Schedule.flight_number' => $this->request->data['Schedulesdetail']['flight_number']);
		  		}else{
		  			$conditions = array('Schedulesdetail.date' => $date, 'flight_number' => $this->request->data['Schedulesdetail']['flight_number']);
		  		}
		  	}
		  	else{
		  		if(empty($this->request->data['Schedulesdetail']['flight_number']) && !empty($this->request->data['Schedulesdetail']['date'])){		  			
		  			$conditions = array(
					    'Schedule.flight_type' => $this->request->data['Schedulesdetail']['flight_type'],
					    'Schedulesdetail.date' => $date);
		  		}
		  		elseif (!empty($this->request->data['Schedulesdetail']['flight_number']) && empty($this->request->data['Schedulesdetail']['date'])){
		  			$conditions = array(
					    'Schedule.flight_type' => $this->request->data['Schedulesdetail']['flight_type'],
					    'Schedule.flight_number' => $this->request->data['Schedulesdetail']['flight_number']);
		  		}
		  		else{
		  			$conditions = array(
		  				'flight_number' => $this->request->data['Schedulesdetail']['flight_number'],
					    'Schedule.flight_type' => $this->request->data['Schedulesdetail']['flight_type'],
					    'Schedulesdetail.date' => $date);
		  		}

		  	}	  	

		  	//Si necesitas el pdf, activa este bloque y antes pon el dopaginator
		  	/*$this->pdfConfig = array(
				'download' => true,
				'filename' => 'agenda_schedule.pdf'
			);*/

		}
		else{
			//primera vez muestra todos los scheduledetails del dia actual
			$currentDate = date('Y-m-d');
			$conditions = array('Schedulesdetail.date' => $currentDate);  		
		}

		$ds_data = $this->dopaginator($conditions, 10, 'Schedulesdetail');
		$this->set(compact('ds_data'));
	}


  /**
  * Funcion que se llama para paginar. Recibe como parametros las condiciones, el limite y el modelo sobre el cual se paginara
  **/
  function dopaginator($conditions, $limit, $model)
  {
    $this->Paginator->settings = array(
        'conditions' => $conditions,
        'limit' => $limit,
        'recursive' =>2,
        'order' => array('Schedulesdetail.estimated_time' => 'asc')
      );
    
    $this->Schedulesdetail->Location->unbindmodel(array('hasMany' => array('Schedulesdetail'))); 
    $this->Schedulesdetail->Driver->unbindmodel(array('hasMany' => array('Schedulesdetail'))); 
    $this->Schedulesdetail->Vehicle->unbindmodel(array('hasMany' => array('Schedulesdetail'))); 
    $this->Schedulesdetail->Schedule->unbindmodel(array('hasMany' => array('Schedulesdetail'))); 

    return $this->Paginator->paginate($model);
  }
  
}

<?php
App::uses('AppController', 'Controller');
/**
 * Schedules Controller
 *
 * @property Schedule $Schedule
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class SchedulesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'RequestHandler');
	var $uses = array('Schedule', 'Airline', 'Driver', 'Vehicle', 'Location', 'Schedulesdetail');

/**
 * index method
 *
 * @return void
 */
	public function index() {

		$conditions = array();

		if ($this->request->is(array('post', 'put'))) {
			if($this->request->data['Schedule']['flight_type'] == 'All'){	
				$conditions = array('Schedule.flight_number' => $this->request->data['Schedule']['flight_number']);

			}else{
				$conditions = array(
				    'Schedule.flight_type' => $this->request->data['Schedule']['flight_type'],
				    'Schedule.flight_number' => $this->request->data['Schedule']['flight_number']);
			}
		}

	    $options = array(
	    	'conditions' => $conditions,
	        'limit' => 20,
	        'recursive' => 0,
	        'order' => array('Schedule.created' => 'DESC')
        );

		$this->Paginator->settings = $options;
		$this->set('schedules', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Schedule->exists($id)) {
			throw new NotFoundException(__('Invalid schedule'));
		}
		$options = array('conditions' => array('Schedule.' . $this->Schedule->primaryKey => $id));

		if ($this->request->is(array('post', 'put'))) {		
			$this->pdfConfig = array(
				'download' => true,
				'filename' => 'schedule_' . $id .'.pdf'
			);
		}

		$this->set('schedule', $this->Schedule->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Schedule->create();
			if ($this->Schedule->save($this->request->data)) {
				$this->Session->setFlash(__('The schedule has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The schedule could not be saved. Please, try again.'));
			}
		}
		$airlines = $this->Schedule->Airline->find('list');
		$this->set(compact('airlines'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Schedule->exists($id)) {
			throw new NotFoundException(__('Invalid schedule'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$this->log($this->request->data);
			if ($this->Schedule->save($this->request->data)) {
				$this->Session->setFlash(__('The schedule has been saved.'), 'flash_good');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The schedule could not has been saved.'), 'flash_bad');
			}
		} else {
			$options = array('conditions' => array('Schedule.' . $this->Schedule->primaryKey => $id));
			$this->request->data = $this->Schedule->find('first', $options);
		}
		$airlines = $this->Schedule->Airline->find('list');
		$this->set(compact('airlines', 'id'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Schedule->id = $id;
		if (!$this->Schedule->exists()) {
			throw new NotFoundException(__('Invalid schedule'));
		}
		//$this->request->allowMethod('post', 'delete');
		if ($this->Schedule->delete()) {
			$this->Session->setFlash(__('The schedule has been deleted.'), 'flash_good');
		} else {
			$this->Session->setFlash(__('The schedule could not be deleted. Please, try again.'), 'flash_bad');
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * addnew method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function addnew($id = null) {
		if ($this->request->is('post')) {
			//$this->log($this->request->data);return;

			$sch_type = 'SCH';
			$user = $this->Auth->user();
			
			if (!$user['is_admin']){
				$sch_type = 'ADD';
			}

			$ds = $this->Schedule->putDataTogether($this->request->data, $sch_type);

			if($ds){
				$this->Session->setFlash(__('Schedule created.'), 'flash_good');
				return $this->redirect(array('action' => 'index'));
			}
			else{
				$this->Session->setFlash(__('The schedule could not be saved. Please, try again.'), 'flash_bad');				
			}
		}

		$airlines = $this->Airline->find('list');
        $locations = $this->Location->find('list');
        $drivers = $this->Driver->find('list');
        $vehicles = $this->Vehicle->find('list');
        $this->set(compact('airlines', 'locations', 'drivers', 'vehicles'));
	}

}
<?php
App::uses('AppController', 'Controller');
App::uses('Group', 'Model');
App::uses('CakeEmail', 'Network/Email'); // this should be placed before your Controller Class prueba


/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class AdministratorsController extends AppController {

/**
 * Components
 *
 * @var array
 */
  var $uses = array('Notificacion', 'Usuario', 'Like', 'Tienda', 'Seguidor','Usuariobanco','Banco');
  public $helpers = array('Js' => array('Jquery'));


	/**
	 * Loguea a un usuario
	 */
	public function login()
	{
		$this->layout = 'store';
		
		if ($this->request->is('post'))
	    {
	        if ($this->Auth->login())
	        {
	            //Para volver al lugar de la peticion original. Lugar de entrada.
	           	//$this->redirect($this->Auth->redirect());
	           	return $this->redirect($this->Auth->redirectUrl());
	           	//$this->redirect(array('controller' => 'usuarios', 'action' => 'entered'));
	        }
	        else
	        { 	
	        	$this->Session->setFlash('Administrator o clave incorrectos.', 'flash_bad', array(), 'auth');
	        }
	    }
	}

	public function logout()
	{
		return $this->redirect($this->Auth->logout());

	}

	public function accede()
	{
		$this->layout = 'store';

	}
	
	public function perfil_usuario()
	{
		$this->layout = 'store';

	}
	
	function afterFilter()
	{
		$includeAfterFilter = array('perfil_usuario');
		if (in_array($this->action,$includeAfterFilter)){
			$this->Session->write('lastUser', 'User');
			$this->Session->delete('lastHome');
			$this->Session->delete('lastStore');
		}
	}
	
}
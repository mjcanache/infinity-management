<?php
/**
 * SchedulesdetailFixture
 *
 */
class SchedulesdetailFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'schedule_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'location_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'airport' => array('type' => 'string', 'null' => false, 'default' => 'MIA', 'length' => 45, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'crew' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'pilot_identification' => array('type' => 'string', 'null' => true, 'default' => 'N/A', 'length' => 45, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'flight_state' => array('type' => 'string', 'null' => false, 'default' => 'N/A', 'length' => 45, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'pickup_crew' => array('type' => 'time', 'null' => true, 'default' => '00:00:00'),
		'drop_crew' => array('type' => 'time', 'null' => true, 'default' => '00:00:00'),
		'driver_id' => array('type' => 'integer', 'null' => false, 'default' => '1', 'unsigned' => false, 'key' => 'index'),
		'vehicle_id' => array('type' => 'integer', 'null' => false, 'default' => '1', 'unsigned' => false, 'key' => 'index'),
		'comments' => array('type' => 'string', 'null' => true, 'default' => 'no comments', 'length' => 500, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'estimated_time' => array('type' => 'time', 'null' => false, 'default' => '00:00:00'),
		'date' => array('type' => 'date', 'null' => true, 'default' => '1900-01-01'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'fk_schedulesdetail_location' => array('column' => 'location_id', 'unique' => 0),
			'fk_schedulesdetail_driver' => array('column' => 'driver_id', 'unique' => 0),
			'fk_schedulesdetail_vehicle' => array('column' => 'vehicle_id', 'unique' => 0),
			'fk_schedulesdetail_schedule' => array('column' => 'schedule_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'schedule_id' => 1,
			'location_id' => 1,
			'airport' => 'Lorem ipsum dolor sit amet',
			'crew' => 1,
			'pilot_identification' => 'Lorem ipsum dolor sit amet',
			'flight_state' => 'Lorem ipsum dolor sit amet',
			'pickup_crew' => '21:47:18',
			'drop_crew' => '21:47:18',
			'driver_id' => 1,
			'vehicle_id' => 1,
			'comments' => 'Lorem ipsum dolor sit amet',
			'estimated_time' => '21:47:18',
			'date' => '2015-12-02'
		),
	);

}

<?php
App::uses('Additionallocation', 'Model');

/**
 * Additionallocation Test Case
 *
 */
class AdditionallocationTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.additionallocation',
		'app.sheduledetails'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Additionallocation = ClassRegistry::init('Additionallocation');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Additionallocation);

		parent::tearDown();
	}

}

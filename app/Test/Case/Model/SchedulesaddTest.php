<?php
App::uses('Schedulesadd', 'Model');

/**
 * Schedulesadd Test Case
 *
 */
class SchedulesaddTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.schedulesadd',
		'app.schedulesdetail',
		'app.schedule',
		'app.airline',
		'app.location',
		'app.driver',
		'app.vehicle'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Schedulesadd = ClassRegistry::init('Schedulesadd');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Schedulesadd);

		parent::tearDown();
	}

}

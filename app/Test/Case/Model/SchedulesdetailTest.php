<?php
App::uses('Schedulesdetail', 'Model');

/**
 * Schedulesdetail Test Case
 *
 */
class SchedulesdetailTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.schedulesdetail',
		'app.schedule',
		'app.airline',
		'app.location',
		'app.driver',
		'app.vehicle',
		'app.schedulesadd'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Schedulesdetail = ClassRegistry::init('Schedulesdetail');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Schedulesdetail);

		parent::tearDown();
	}

}

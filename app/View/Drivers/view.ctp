<div class="drivers view">
<h2><?php echo __('Driver'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($driver['Driver']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Document'); ?></dt>
		<dd>
			<?php echo h($driver['Driver']['document']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($driver['Driver']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Phone'); ?></dt>
		<dd>
			<?php echo h($driver['Driver']['phone']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Enable'); ?></dt>
		<dd>
			<?php echo h($driver['Driver']['is_enable']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Driver'), array('action' => 'edit', $driver['Driver']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Driver'), array('action' => 'delete', $driver['Driver']['id']), array(), __('Are you sure you want to delete # %s?', $driver['Driver']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Drivers'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Driver'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Schedulesdetails'), array('controller' => 'schedulesdetails', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Schedulesdetail'), array('controller' => 'schedulesdetails', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Schedulesdetails'); ?></h3>
	<?php if (!empty($driver['Schedulesdetail'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Schedule Id'); ?></th>
		<th><?php echo __('Location Id'); ?></th>
		<th><?php echo __('Airport'); ?></th>
		<th><?php echo __('Crew'); ?></th>
		<th><?php echo __('Flight State'); ?></th>
		<th><?php echo __('Pickup Crew'); ?></th>
		<th><?php echo __('Drop Crew'); ?></th>
		<th><?php echo __('Driver Id'); ?></th>
		<th><?php echo __('Vehicle Id'); ?></th>
		<th><?php echo __('Comments'); ?></th>
		<th><?php echo __('Estimated Time'); ?></th>
		<th><?php echo __('Date'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($driver['Schedulesdetail'] as $schedulesdetail): ?>
		<tr>
			<td><?php echo $schedulesdetail['id']; ?></td>
			<td><?php echo $schedulesdetail['schedule_id']; ?></td>
			<td><?php echo $schedulesdetail['location_id']; ?></td>
			<td><?php echo $schedulesdetail['airport']; ?></td>
			<td><?php echo $schedulesdetail['crew']; ?></td>
			<td><?php echo $schedulesdetail['flight_state']; ?></td>
			<td><?php echo $schedulesdetail['pickup_crew']; ?></td>
			<td><?php echo $schedulesdetail['drop_crew']; ?></td>
			<td><?php echo $schedulesdetail['driver_id']; ?></td>
			<td><?php echo $schedulesdetail['vehicle_id']; ?></td>
			<td><?php echo $schedulesdetail['comments']; ?></td>
			<td><?php echo $schedulesdetail['estimated_time']; ?></td>
			<td><?php echo $schedulesdetail['date']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'schedulesdetails', 'action' => 'view', $schedulesdetail['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'schedulesdetails', 'action' => 'edit', $schedulesdetail['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'schedulesdetails', 'action' => 'delete', $schedulesdetail['id']), array(), __('Are you sure you want to delete # %s?', $schedulesdetail['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Schedulesdetail'), array('controller' => 'schedulesdetails', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>

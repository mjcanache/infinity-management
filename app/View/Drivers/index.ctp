<div class="row">
	<div class="col-lg-12">
	  <h3 class="page-header"><i class="fa fa-file-text-o"></i> Drivers</h3>
	</div>
</div>

<div class="row">
  <div class="col-lg-6"><?php
        	echo $this->Html->link('<span class="icon_plus_alt2"></span> Add new', array(
			'controller' => 'drivers',
	        'action'=>'add'
		    ),
		    array(
		        'class'=>'btn btn-success btn-sm',
		        'style' => 'margin-bottom: 10px;',
		        'escape'=>false
		    ));?>   
      <section class="panel">      
          <table class="table table-striped table-advance table-hover">
           <tbody>
				<tr>
					<th><?php echo $this->Paginator->sort('document'); ?></th>
					<th><?php echo $this->Paginator->sort('name'); ?></th>
					<th><?php echo $this->Paginator->sort('phone'); ?></th>
					<th><?php echo $this->Paginator->sort('is_enable'); ?></th>
					<th class="actions"><?php echo __('Actions'); ?></th>  
				</tr>
				<?php foreach ($drivers as $driver):
					if($driver['Driver']['document'] == 'N/A'){
						continue;
					}
					else{?>
						<tr>
		              		<td><?php echo $driver['Driver']['document'];?>&nbsp;</td>
							<td><?php echo $driver['Driver']['name'];?>&nbsp;</td>
							<td><?php echo $driver['Driver']['phone']; ?>&nbsp;</td>
							<td><?php $action = (Configure::read('INIFINITY.'.$driver['Driver']['is_enable'].'.ENABLE')) ? 'YES' : 'NO'; echo $action; ?></td>
							<td>
			                    <div class="btn-group"><?php
			                    	echo $this->Html->link('<i class="icon_pencil-edit"></i>', array(
									        'action'=>'edit',
									         $driver['Driver']['id']
									    ),
									    array(
									        'class'=>'btn btn-primary',
									        'escape'=>false
									    ));?>		              		
								</div>
			              	</td>
						</tr>
					
				 

				<?php }endforeach; ?>
           </tbody>
        </table>
		
      </section>
  </div>
</div>
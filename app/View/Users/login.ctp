<?php echo $this->Form->create('Users', array(
    'url' => array('controller' => 'users', 'action' => 'login'),
    'class' => 'login-form',
    'inputDefaults' => array(
        'class' => '',
        'div' => false,
        'label' => false
        )));
  ?>
  <div class="login-wrap">
   
    <?php echo $this->Html->image('logo.png', array('alt' => 'logo', 'style'=>'margin-bottom: 10px;width: 100%;height: auto;'));?>
    <div class="input-group">
            <span class="input-group-addon"><i class="icon_profile"></i></span>
            <?php echo $this->Form->input('login', array('class'=>'form-control', 'placeholder' => __('Email')));?>
        </div>
        <div class="input-group">
            <span class="input-group-addon"><i class="icon_key_alt"></i></span>
            <?php echo $this->Form->input('password', array('class'=>'form-control', 'placeholder' => __('Password')));?>
        </div>
        <button class="btn btn-primary btn-lg btn-block" type="submit">Login</button>
  </div>

<?php echo $this->Form->end();?>
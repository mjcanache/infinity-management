<div class="alert alert-success">
  <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">×</a>
  <strong>Success!</strong> <?php echo $message;?>
</div>
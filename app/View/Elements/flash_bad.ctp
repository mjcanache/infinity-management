<div class="alert alert-danger">
  <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">×</a>
  <strong>Ops!</strong> <?php echo $message;?>
</div>
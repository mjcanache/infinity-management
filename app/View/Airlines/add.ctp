<div class="row">
	<div class="col-lg-12">
	  <h3 class="page-header"><i class="fa fa-file-text-o"></i> Add new airline</h3>
	</div>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading">
            Form Elements
          </header>
          <div class="panel-body"><?php
            echo $this->Form->create('Airline', array(
                              'class' => 'form-horizontal',
                              'label' => false ));?>


           <div class="form-group">
                <label class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-10">
                        <?= $this->Form->input('name', array('class' => 'form-control', 'label' => false)) ?>
                   </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Code</label>
                    <div class="col-sm-10">
                        <?= $this->Form->input('code', array('class' => 'form-control', 'label' => false)) ?>
                   </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Rate</label>
                    <div class="col-sm-10">
                        <?= $this->Form->input('rate', array('class' => 'form-control', 'label' => false, 'min' =>'1')) ?>
                   </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Is enable</label>
                    <div class="col-sm-1">
                      <?php echo $this->Form->input('is_enable', array('class' => 'form-control', 'label' => false)) ?>
                   </div>
            </div>      

            <button type="submit" class="btn btn-primary" style="float:right;">Submit</button>
            <?php echo $this->Form->end();?>
                  
        </section>
    </div>
</div>
<div class="row">
	<div class="col-lg-12">
	  <h3 class="page-header"><i class="fa fa-file-text-o"></i> View airline</h3>
	</div>
</div>


<div class="row">
  <div class="col-lg-6">   
      <section class="panel">      
          <table class="table table-striped table-advance table-hover">
           <tbody>
				<tr>
					<th><?php echo __('Name'); ?></th>
					<th><?php echo __('Code'); ?></th>
					<?php if($user['is_admin']){?>
					<th><?php echo __('Rate'); ?> (USD)</th>
					<?php }?>
					<th><?php echo __('Is enable'); ?> (USD)</th>
					<th class="actions"><?php echo __('Actions'); ?></th>  
				</tr>
				
             	<tr>
              		<td><?php echo $airline['Airline']['name'];?>&nbsp;</td>
					<td><?php echo h($airline['Airline']['code']);?>&nbsp;</td>
					<?php if($user['is_admin']){?>
					<td><?php echo  h($airline['Airline']['rate']);?>&nbsp;</td>
					<?php }?>
					<td><?php $action = (Configure::read('INIFINITY.'.$airline['Airline']['is_enable'].'.ENABLE')) ? 'YES' : 'NO'; echo $action; ?></td>
					<td>
	                    <div class="btn-group"><?php
	                    	echo $this->Html->link('<i class="icon_pencil-edit"></i>', array(
							        'action'=>'edit',
							         $airline['Airline']['id']
							    ),
							    array(
							        'class'=>'btn btn-primary',
							        'escape'=>false
							    ));?>		              		
						</div>
	              	</td>
				</tr>
				
           </tbody>
        </table>
		
      </section>
  </div>
</div>
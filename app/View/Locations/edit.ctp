<div class="row">
	<div class="col-lg-12">
	  <h3 class="page-header"><i class="fa fa-file-text-o"></i> Edit hotel</h3>
	</div>
</div>


<div class="row">
    <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading">
            Form Elements
          </header>
          <div class="panel-body"><?php
            echo $this->Form->create('Location', array(
            				  //'url' => array('controller' => 'locations', 'action' => 'edit'),
                              'class' => 'form-horizontal',
                              'label' => false ));
                               echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$id)); ?>


           <div class="form-group">
                <label class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-10">
                       <?php echo $this->Form->input('name', array('class' => 'form-control', 'label' => false)) ?>                      
                   </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Short name</label>
                    <div class="col-sm-10">
                    	<?php echo $this->Form->input('short_name', array('class' => 'form-control', 'label' => false)) ?>
                   </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Address</label>
                    <div class="col-sm-10">
                        <?= $this->Form->input('address', array('class' => 'form-control', 'label' => false)) ?>
                   </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Contact</label>
                    <div class="col-sm-10">
                        <?= $this->Form->input('contact', array('class' => 'form-control', 'label' => false)) ?>
                   </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Phone</label>
                    <div class="col-sm-10">
                        <?= $this->Form->input('phone', array('class' => 'form-control', 'label' => false)) ?>
                   </div>
            </div>            

            <div class="form-group">
                <label class="col-sm-2 control-label">Is enable</label>
                    <div class="col-sm-1">
                    	<?php echo $this->Form->input('is_enable', array('class' => 'form-control', 'label' => false)) ?>
                   </div>
            </div>

            <button type="submit" class="btn btn-primary" style="float:right;">Submit</button>
            <?php echo $this->Form->end();?>
                  
        </section>
    </div>
</div>
<div class="row">
	<div class="col-lg-12">
	  <h3 class="page-header"><i class="fa fa-file-text-o"></i> View hotel</h3>
	</div>
</div>


<div class="row">
  <div class="col-lg-10">   
      <section class="panel">      
          <table class="table table-striped table-advance table-hover">
           <tbody>
				<tr>
					<th><?php echo __('Name'); ?></th>
					<th><?php echo __('Short name'); ?></th>
					<th><?php echo __('address'); ?></th>
					<th><?php echo __('contact'); ?></th>
					<th><?php echo __('phone'); ?></th>
					<th><?php echo __('Is enable'); ?></th>
					<th class="actions"><?php echo __('Actions'); ?></th>  
				</tr>
				
             	<tr>
              		<td><?php echo h($location['Location']['name']);?>&nbsp;</td>
					<td><?php echo h($location['Location']['short_name']);?>&nbsp;</td>
					<td><?php echo h($location['Location']['address']);?>&nbsp;</td>
					<td><?php echo h($location['Location']['contact']);?>&nbsp;</td>
					<td><?php echo h($location['Location']['phone']);?>&nbsp;</td>
					<td><?php $action = (Configure::read('INIFINITY.'.$location['Location']['is_enable'].'.ENABLE')) ? 'YES' : 'NO'; echo $action; ?></td>
					<td>
	                    <div class="btn-group"><?php
	                    	echo $this->Html->link('<i class="icon_pencil-edit"></i>', array(
							        'action'=>'edit',
							         $location['Location']['id']
							    ),
							    array(
							        'class'=>'btn btn-primary',
							        'escape'=>false
							    ));?>		              		
						</div>
	              	</td>
				</tr>
				
           </tbody>
        </table>
		
      </section>
  </div>
</div>
<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Infinity';
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
  <?php
    echo $this->Html->charset();
    echo $this->Html->meta('robots', 'index,follow');
    echo  $this->Html->meta('description', 'Infinity');
    //echo $this->Html->meta('icon');
  ?>
  <title>
    <?php echo $cakeDescription ?>
  </title>

  <?php
    //Mobile Specific Metas
    echo  $this->Html->meta('HandheldFriendly', 'True');
    echo  $this->Html->meta('MobileOptimized', '320');
    echo  $this->Html->meta('viewport', 'width=device-width, initial-scale=1, maximum-scale=1');

    //Custom CSS
    echo $this->Html->css('bootstrap.min');    
    echo $this->Html->css('bootstrap-theme');
    echo $this->Html->css('elegant-icons-style');    
    echo $this->Html->css('font-awesome.min');    
    echo $this->Html->css('bootstrap-datepicker');
    echo $this->Html->css('style-responsive'); 
    echo $this->Html->css('owl.carousel'); 
    echo $this->Html->css('widgets'); 
    echo $this->Html->css('jquery-ui-1.10.4.min');  
     
    //fetch
    echo $this->fetch('meta');
    echo $this->fetch('css');
    echo $this->fetch('script');

    echo $this->Html->script('jquery-1.9.1.min'); // es el cdn bajado
    echo $this->Html->script('bootstrap.min'); // es el cdn bajado
    echo $this->Html->css('bootstrap-3.min');// es el cdn bajado
    echo $this->Html->css('bootstrap-datepicker-cdn');// es el cdn bajado
    
  ?>

 <!--   <script src="//code.jquery.com/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://eternicode.github.io/bootstrap-datepicker/bootstrap-datepicker/css/datepicker3.css">-->

    <?php echo $this->Html->css('style');?>
  <script type="text/javascript">   
   $(document).ready(function(){
      $('#sandbox-container').datepicker({
          clearBtn: true,
          multidate: true,
          todayHighlight: true
         });

      $('#sandbox-container-solo').datepicker({
          clearBtn: true,
          todayHighlight: true
         });
    });
  </script>

</head>
<body>

  <!-- container section start -->
  <section id="container" class="">
      <!--header start-->
      <header class="header dark-bg">
            <div class="toggle-nav">
                <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>
            </div>

            <!--logo start-->
            <a href="index.html" class="logo">Infinity Management Task</a>
            <!--logo end-->

             <div class="top-nav notification-row">                
                <!-- notificatoin dropdown start-->
                <ul class="nav pull-right top-menu">
                    
                    <!-- user login dropdown start-->
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="profile-ava">
                                <img alt="" src="img/avatar1_small.jpg">
                            </span>
                            <span class="username">Hi <?php echo $user['name'];?></span>
                            <b class="caret"></b>
                        </a>
                    </li>
                    <!-- user login dropdown end -->
                </ul>
                <!-- notificatoin dropdown end-->
            </div>
      </header>      
      <!--header end-->

      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu">                
                  <li class="active"><?php 
                    echo $this->Html->link('<i class="icon_house_alt"></i><span>Dashboard</span>', array(
                                  'controller' => 'pages',
                                  'action'=>'home'
                              ),
                              array(
                                  'escape'=>false
                              ));?>                       
                  </li>                  
                  <li class="sub-menu">
                      <a href="javascript:;" class="">
                          <i class="icon_document_alt"></i>
                          <span>Schedules</span>
                          <span class="menu-arrow arrow_carrot-right"></span>
                      </a>
                      <ul class="sub">
                          <li><?php echo $this->Html->link(__('Add new'), array('controller' => 'schedules', 'action' => 'addnew'));?></li> <?php if ($user['is_admin']){?>                         
                          <li><?php echo $this->Html->link(__('List all'), array('controller' => 'schedules', 'action' => 'index'));?></li>
                           <?php } ?>   
                      </ul>
                  </li>
                     
                  <li class="sub-menu">
                      <a href="javascript:;" class="">
                          <i class="fa fa-th-list"></i>
                          <span>Dispatch</span>
                          <span class="menu-arrow arrow_carrot-right"></span>
                      </a>
                      <ul class="sub">
                          <li><?php echo $this->Html->link(__('Agenda'), array('controller' => 'schedulesdetails', 'action' => 'agenda')); ?></li>
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" class="">
                          <i class="icon_cog"></i>
                          <span>Settings</span>
                          <span class="menu-arrow arrow_carrot-right"></span>
                      </a>
                      <ul class="sub">
                          <li><?php echo $this->Html->link(__('Airlines'), array('controller' => 'airlines', 'action' => 'index')); ?></li>
                          <li><?php echo $this->Html->link(__('Hotels'), array('controller' => 'locations', 'action' => 'index')); ?></li>
                          <li><?php echo $this->Html->link(__('Drivers'), array('controller' => 'drivers', 'action' => 'index')); ?></li>                         
                          <li><?php echo $this->Html->link(__('Vehicles'), array('controller' => 'vehicles', 'action' => 'index')); ?></li>
                      </ul>
                  </li>
                  <li><?php
                    echo $this->Html->link('<i class="fa fa-sign-out"></i><span>Logout</span>', array(
                                    'controller' => 'users',
                                    'action'=>'logout'
                                ),
                                array(
                                    'escape'=>false
                                ));?>

                  </li>
                                    
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->

      <!--main content start-->
      <section id="main-content">
          <section class="wrapper"><?php      
            echo $this->Session->flash();
            echo $this->fetch('content');?>
          </section>
      </section>
      <!--main content end-->
  </section>
  <!-- container section end -->

  <?php
  //js
    echo $this->Html->script('jquery.min');
    echo $this->Html->script('jquery-ui-1.10.4.min');
    echo $this->Html->script('bootstrap-datepicker');    
    //echo $this->Html->script('jquery-1.8.3.min');    
   //echo $this->Html->script('jquery-ui-1.9.2.custom.min');    
    echo $this->Html->script('bootstrap.min');    
    echo $this->Html->script('jquery.scrollTo.min');    
    echo $this->Html->script('jquery.nicescroll');
    echo $this->Html->script('jquery.customSelect.min');    
    echo $this->Html->script('scripts');?>

</body>
</html>
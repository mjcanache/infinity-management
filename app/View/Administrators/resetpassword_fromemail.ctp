     <?php
		echo $this->Form->create('Usuario', array(
		'url' => array('controller' => 'usuarios', 'action' => 'resetpassword_fromemail'),
		'class' => null));


		echo $this->Form->hidden('Usuario.clave_recuperacion');
		?>

		<h2><?php echo __('RESETEAR CLAVE');?></h2>

		<p><?php
			echo $this->Form->input('Usuario.login', array('label'=> 'Correo', 'type' => 'email', 'placeholder' => __('Correo electrónico')));?>
		</p>

		<p><?php
			echo $this->Form->input('Usuario.password', array('label'=> 'Clave', 'placeholder' => __('Clave')));?>
		</p>

		<p><?php
			echo $this->Form->input('Usuario.password2', array('label'=> 'Repita su clave', 'type' => 'password', 'placeholder' => __('Repita su clave')));?>
		</p>

		<p><?php
				echo $this->Form->button(__('Cambiar'), array('class' => 'boton_morado margen_boton'));?>
		</p>

	<?php
		echo $this->Form->end(); ?>
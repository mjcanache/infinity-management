<div class="row">
	<div class="col-lg-12">
	  <h3 class="page-header"><i class="fa fa-file-text-o"></i> Vehicles</h3>
	</div>
</div>

<div class="row">
  <div class="col-lg-6"><?php
        	echo $this->Html->link('<span class="icon_plus_alt2"></span> Add new', array(
			'controller' => 'vehicles',
	        'action'=>'add'
		    ),
		    array(
		        'class'=>'btn btn-success btn-sm',
		        'style' => 'margin-bottom: 10px;',
		        'escape'=>false
		    ));?>   
      <section class="panel">      
          <table class="table table-striped table-advance table-hover">
           <tbody>
				<tr>
					<th><?php echo $this->Paginator->sort('brand'); ?></th>
					<th><?php echo $this->Paginator->sort('model'); ?></th>
					<th><?php echo $this->Paginator->sort('year'); ?></th>
					<th><?php echo $this->Paginator->sort('vin'); ?></th>
					<th><?php echo $this->Paginator->sort('is_enable'); ?></th>
					<th class="actions"><?php echo __('Actions'); ?></th>  
				</tr>
				<?php foreach ($vehicles as $vehicle): 
					if($vehicle['Vehicle']['model'] == 'N/A'){
						continue;
					}
					else{?>
						<tr>
		              		<td><?php echo $vehicle['Vehicle']['brand'];?>&nbsp;</td>
							<td><?php echo $vehicle['Vehicle']['model'];?>&nbsp;</td>
							<td><?php echo $vehicle['Vehicle']['year']; ?>&nbsp;</td>
							<td><?php echo $vehicle['Vehicle']['vin']; ?>&nbsp;</td>							
							<td><?php $action = (Configure::read('INIFINITY.'.$vehicle['Vehicle']['is_enable'].'.ENABLE')) ? 'YES' : 'NO'; echo $action; ?></td>
							<td>
			                    <div class="btn-group"><?php
			                    	echo $this->Html->link('<i class="icon_pencil-edit"></i>', array(
									        'action'=>'edit',
									         $vehicle['Vehicle']['id']
									    ),
									    array(
									        'class'=>'btn btn-primary',
									        'escape'=>false
									    ));?>		              		
								</div>
			              	</td>
						</tr>
				<?php }endforeach; ?>
           </tbody>
        </table>
		
      </section>
  </div>
</div>
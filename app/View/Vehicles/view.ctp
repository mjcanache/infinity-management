<div class="row">
	<div class="col-lg-12">
	  <h3 class="page-header"><i class="fa fa-file-text-o"></i> View vehicle</h3>
	</div>
</div>

<div class="row">
  <div class="col-lg-6">   
      <section class="panel">      
          <table class="table table-striped table-advance table-hover">
           <tbody>
				<tr>
					<th><?php echo __('Brand'); ?></th>
					<th><?php echo __('Model'); ?></th>
					<th><?php echo __('Year'); ?> </th>
					<th><?php echo __('Vin'); ?></th>
					<th><?php echo __('Is Enable'); ?></th>
					<th class="actions"><?php echo __('Actions'); ?></th>  
				</tr>
				
             	<tr>
              		<td><?php echo h($vehicle['Vehicle']['brand']);?>&nbsp;</td>
					<td><?php echo h($vehicle['Vehicle']['model']); ?>&nbsp;</td>
					<td><?php echo h($vehicle['Vehicle']['year']);?>&nbsp;</td>
					<td><?php echo h($vehicle['Vehicle']['vin']); ?>&nbsp;</td>
					<td><?php echo h($vehicle['Vehicle']['is_enable']);?>&nbsp;</td>
					<td><?php $action = (Configure::read('INIFINITY.'.$vehicle['Vehicle']['is_enable'].'.ENABLE')) ? 'YES' : 'NO'; echo $action; ?></td>
					<td>
	                    <div class="btn-group"><?php
	                    	echo $this->Html->link('<i class="icon_pencil-edit"></i>', array(
							        'action'=>'edit',
							         $vehicle['Vehicle']['id']
							    ),
							    array(
							        'class'=>'btn btn-primary',
							        'escape'=>false
							    ));?>		              		
						</div>
	              	</td>
				</tr>
				
           </tbody>
        </table>
		
      </section>
  </div>
</div>
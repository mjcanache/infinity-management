<div class="row">
	<div class="col-lg-6">
	  <h3 class="page-header"><i class="fa fa-file-text-o"></i> Agenda</h3>
	</div>
	<div class="col-lg-6"><?php 
		//echo $this->Html->link('<i class="fa fa-share"></i> Export to PDF', array('action'=>'agenda','ext' => 'pdf'),  array('escape'=>false,'style' => 'float:right'));?> 
	</div>
</div>

<div class="row">
  <div class="col-lg-12">
      <section class="panel">
          <header class="panel-heading">
              Search by date, flight number and flight type 
          </header>
          <div class="panel-body"><?php
                echo $this->Form->create('Schedulesdetail', array(
                                    'url' => array('controller' => 'schedulesdetails', 'action' => 'agenda'),
                                    'class' => 'form-inline',
                                    'label' => false ));?>
                      <!-- Para una sola fecha del calendario: sandbox-container-solo -->
                      <div class="form-group">
                          <label class="sr-only">Date</label>                             
                          <?= $this->Form->input('date', array('class' => 'form-control', 'label' => false, 'id'=>'sandbox-container', 'type'=>'text', 'placeholder' => 'Date')) ?>                        
                      </div>

                      <div class="form-group">
                              <div class="col-sm-10">
                                  <?= $this->Form->input('flight_number', array('class' => 'form-control', 'label' => false, 'placeholder' => 'Flight number')) ?>
                              </div>
                          </div>

                      <div class="form-group">
                          <label class="sr-only">Filter</label>                             
                          <?= $this->Form->input('flight_type', array('class' => 'form-control', 'label' => false, 'options' => Configure::read('INIFINITY.FLIGHT_TYPE_SEARCH'))) ?>                       
                      </div>

	                  <button type="submit" class="btn btn-primary">search</button>
               <?php echo $this->Form->end();?>
          </div>
      </section>

  </div>
</div>


<div class="row">
  <div class="col-lg-12">
      <section class="panel">      
          <table class="table table-stripedx table-advance table-hoverx">
           <tbody>
              <tr>
              	<th><?php echo $this->Paginator->sort('Schedule');?></th>
              	<th><?php echo $this->Paginator->sort('Flight type');?></th>
              	<th><?php echo $this->Paginator->sort('Airline');?></th>
              	<th><?php echo $this->Paginator->sort('Flight number');?></th>
              	<th><?php echo $this->Paginator->sort('Pickup at'); ?></th>
              	<th><?php echo $this->Paginator->sort('Drop at');?></th>
				<th><?php echo $this->Paginator->sort('Estimated pickup time');?></th>
				<th><?php echo $this->Paginator->sort('Pilot ID');?></th>
				<th><?php echo $this->Paginator->sort('Crew');?></th>
				<th><?php echo $this->Paginator->sort('Pickup time');?></th>
				<th><?php echo $this->Paginator->sort('Drop time');?></th>
				<th><?php echo $this->Paginator->sort('Flight state');?></th>	
				<th><?php echo $this->Paginator->sort('Driver');?></th>		
				<th><?php echo $this->Paginator->sort('Vehicle');?></th>	
				<th><?php echo $this->Paginator->sort('Date');?></th>						
				<th class="actions"><?php echo __('Actions'); ?></th>    
              </tr> 
			<tbody>
			<?php foreach ($ds_data as $data):
				$pickup_at = null;
				$drop_at = null;
				$class = '';
				if($data['Schedule']['flight_type'] == 'Arrived'){
					$pickup_at = $data['Schedulesdetail']['airport'];
					$drop_at = $data['Location']['short_name'];
				}
				else if ($data['Schedule']['flight_type'] == 'Departure'){
					$pickup_at = $data['Location']['short_name'];
					$drop_at = $data['Schedulesdetail']['airport'];
					$class = 'danger';
				}

				$flight_type = null;
              	if($data['Schedule']['flight_type'] == 'Arrived'){
              		$flight_type = '<span class="label label-success">'.$data['Schedule']['flight_type'].'</span>';
              	}
              	else{
              		$flight_type = '<span class="label label-danger">'.$data['Schedule']['flight_type'].'</span>';
              	}

              	if($data['Schedulesadd']['id'] != 0){
              		$pickup_at = $data['Schedulesadd']['pickup_at'];
              		$drop_at =  $data['Schedulesadd']['drop_at'];
              	}
			 	?>  
              <tr class=<?php echo $class;?>>
              	<td><?php echo $data['Schedule']['sch_type']; ?>&nbsp;</td>
              	<td><?php echo $flight_type; ?>&nbsp;</td>
	      		<td><?php echo $data['Schedule']['Airline']['code'];?>&nbsp;</td>
	      		<td><?php echo $data['Schedule']['flight_number']; ?>&nbsp;</td>
	      		<td><?php echo $pickup_at;?>&nbsp;</td>
				<td><?php echo $drop_at;?></td>
				<td><?php echo $data['Schedulesdetail']['estimated_time'];?>&nbsp;</td>					
				<td><?php echo $data['Schedulesdetail']['pilot_identification']; ?>&nbsp;</td>
				<td><?php echo $data['Schedulesdetail']['crew']; ?>&nbsp;</td>
				<td><?php echo $data['Schedulesdetail']['pickup_crew']; ?>&nbsp;</td>
				<td><?php echo $data['Schedulesdetail']['drop_crew']; ?>&nbsp;</td>		
				<td><?php echo $data['Schedulesdetail']['flight_state']; ?>&nbsp;</td>	
				<td><?php echo $data['Driver']['name']; ?>&nbsp;</td>		
				<td><?php echo $data['Vehicle']['model']; ?>&nbsp;</td>				
				<td><?php echo $data['Schedulesdetail']['date']; ?>&nbsp;</td>					
				<td>
	                <div class="btn-group"><?php
	                	echo $this->Html->link('<i class="icon_pencil-edit"></i>', array(
						        'action'=>'edit',
						         $data['Schedulesdetail']['id'],
						         $data['Schedule']['id']
						    ),
						    array(
						        'class'=>'btn btn-primary',
						        'escape'=>false
						    ));

	                	echo $this->Html->link('<i class="icon_close_alt2"></i>', array(
						        'action'=>'delete',
						        $data['Schedulesdetail']['id']
						    ),
						    array(
						        'class'=>'btn btn-primary',
						        'escape'=>false
						    ));?> 		              		
					</div>
	          	</td>
			</tr> 
			<?php endforeach;?>                        
           </tbody>
        </table>
      </section>
  </div>
</div>

<div class="row">
	<div class="col-lg-12" style="text-align:right;">
		<p class="breadcrumb"><?php
			echo $this->Paginator->counter(array(
			'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
			));
			?>	</p>
			<div class="paging">
			<?php
				echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled')).' ';
				echo $this->Paginator->numbers(array('separator' => ' '));
				echo $this->Paginator->next(__(' next') . ' >', array(), null, array('class' => 'next disabled'));
			?>
			</div>
	</div>
</div>
<div class="row" style="margin-left: 20px">
	<div class="col-lg-6">
	  <h3 class="page-header">Agenda</h3>
	</div>
</div>

<div class="row" style="margin-left: 20px">
  <div class="col-lg-12">
      <section class="panel">      
          <table style="font-family: sans-serif; font-size: 12px; margin-bottom:20px;">
           <tbody>
              <tr>
              	<th style="background-color: #286090; color: white;font-weight: normal; padding-top: 10px; padding-left: 10px; padding-right: 10px; text-align: center;"><?php echo 'Schedule';?></th>
              	<th style="background-color: #286090; color: white;font-weight: normal; padding-top: 10px; padding-left: 10px; padding-right: 10px; text-align: center;"><?php echo 'Flight type';?></th>
              	<th style="background-color: #286090; color: white;font-weight: normal; padding-top: 10px; padding-left: 10px; padding-right: 10px; text-align: center;"><?php echo 'Airline';?></th>
              	<th style="background-color: #286090; color: white;font-weight: normal; padding-top: 10px; padding-left: 10px; padding-right: 10px; text-align: center;"><?php echo 'Flight number';?></th>
              	<th style="background-color: #286090; color: white;font-weight: normal; padding-top: 10px; padding-left: 10px; padding-right: 10px; text-align: center;"><?php echo 'Pickup at';?></th>
              	<th style="background-color: #286090; color: white;font-weight: normal; padding-top: 10px; padding-left: 10px; padding-right: 10px; text-align: center;"><?php echo 'Drop at';?></th>
				<th style="background-color: #286090; color: white;font-weight: normal; padding-top: 10px; padding-left: 10px; padding-right: 10px; text-align: center;"><?php echo 'Estimated pickup time';?></th>
				<th style="background-color: #286090; color: white;font-weight: normal; padding-top: 10px; padding-left: 10px; padding-right: 10px; text-align: center;"><?php echo 'Pilot ID';?></th>
				<th style="background-color: #286090; color: white;font-weight: normal; padding-top: 10px; padding-left: 10px; padding-right: 10px; text-align: center;"><?php echo 'Crew';?></th>
				<th style="background-color: #286090; color: white;font-weight: normal; padding-top: 10px; padding-left: 10px; padding-right: 10px; text-align: center;"><?php echo 'Pickup time';?></th>
				<th style="background-color: #286090; color: white;font-weight: normal; padding-top: 10px; padding-left: 10px; padding-right: 10px; text-align: center;"><?php echo 'Drop time';?></th>
				<th style="background-color: #286090; color: white;font-weight: normal; padding-top: 10px; padding-left: 10px; padding-right: 10px; text-align: center;"><?php echo 'Flight state';?></th>	
				<th style="background-color: #286090; color: white;font-weight: normal; padding-top: 10px; padding-left: 10px; padding-right: 10px; text-align: center;"><?php echo 'Driver';?></th>		
				<th style="background-color: #286090; color: white;font-weight: normal; padding-top: 10px; padding-left: 10px; padding-right: 10px; text-align: center;"><?php echo 'Vehicle';?></th>    
              </tr> 
			<tbody>
			<?php foreach ($ds_data as $data):
				$pickup_at = null;
				$drop_at = null;
				$class = '';
				if($data['Schedule']['flight_type'] == 'Arrived'){
					$pickup_at = $data['Schedulesdetail']['airport'];
					$drop_at = $data['Location']['short_name'];
				}
				else if ($data['Schedule']['flight_type'] == 'Departure'){
					$pickup_at = $data['Location']['short_name'];
					$drop_at = $data['Schedulesdetail']['airport'];
					$class = 'danger';
				}

				$flight_type = null;
              	if($data['Schedule']['flight_type'] == 'Arrived'){
              		$flight_type = '<span class="label label-success">'.$data['Schedule']['flight_type'].'</span>';
              	}
              	else{
              		$flight_type = '<span class="label label-danger">'.$data['Schedule']['flight_type'].'</span>';
              	}
			 ?>  
              <tr class=<?php echo $class;?>>
              	<td style="padding-top: 10px; padding-left: 20px;"><?php echo $data['Schedule']['sch_type']; ?>&nbsp;</td>
              	<td style="padding-top: 10px; padding-left: 20px;"><?php echo $flight_type; ?>&nbsp;</td>
	      		<td style="padding-top: 10px; padding-left: 20px;"><?php echo $data['Schedule']['Airline']['code'];?>&nbsp;</td>
	      		<td style="padding-top: 10px; padding-left: 20px;"><?php echo $data['Schedule']['flight_number']; ?>&nbsp;</td>
	      		<td style="padding-top: 10px; padding-left: 20px;"><?php echo $pickup_at;?>&nbsp;</td>
				<td style="padding-top: 10px; padding-left: 20px;"><?php echo $drop_at;?></td>
				<td style="padding-top: 10px; padding-left: 20px;"><?php echo $data['Schedulesdetail']['estimated_time'];?>&nbsp;</td>				
				<td style="padding-top: 10px; padding-left: 20px;"><?php echo $data['Schedulesdetail']['pilot_identification']; ?>&nbsp;</td>
				<td style="padding-top: 10px; padding-left: 20px;"><?php echo $data['Schedulesdetail']['crew']; ?>&nbsp;</td>
				<td style="padding-top: 10px; padding-left: 20px;"><?php echo $data['Schedulesdetail']['pickup_crew']; ?>&nbsp;</td>
				<td style="padding-top: 10px; padding-left: 20px;"><?php echo $data['Schedulesdetail']['drop_crew']; ?>&nbsp;</td>		
				<td style="padding-top: 10px; padding-left: 20px;"><?php echo $data['Schedulesdetail']['flight_state']; ?>&nbsp;</td>	
				<td style="padding-top: 10px; padding-left: 20px;"><?php echo $data['Driver']['name']; ?>&nbsp;</td>		
				<td style="padding-top: 10px; padding-left: 20px;"><?php echo $data['Vehicle']['model']; ?>&nbsp;</td>	
			</tr> 
			<?php endforeach;?>                        
           </tbody>
        </table>
      </section>
  </div>
</div>
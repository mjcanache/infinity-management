<div class="row">
	<div class="col-lg-12">
	  <h3 class="page-header"><i class="fa fa-file-text-o"></i> View schedule details</h3>
	</div>
</div>


<div class="row">
  <div class="col-lg-12">
      <section class="panel">      
          <table class="table table-striped table-advance table-hover">
           <tbody>
              <tr>
              	<th><?php echo 'Date';?></th>
              	<th><?php echo 'Hotel'; ?></th>
              	<th><?php echo 'Airport';?></th>
				<th><?php echo 'Crew';?></th>
				<th><?php echo 'Pilot ID';?></th>
				<th><?php echo 'Flight state';?></th>
				<th><?php echo 'Pickup crew';?></th>
				<th><?php echo 'Drop crew';?></th>
				<th><?php echo 'Driver';?></th>
				<th><?php echo 'Vehicle';?></th>
				<th><?php echo 'Comments';?></th>
				<th><?php echo 'Estimated time';?></th>				
				<th class="actions"><?php echo __('Actions'); ?></th>    
              </tr>

              	<tr>
              		<td><?php echo $schedulesdetail['Schedulesdetail']['date'];?>&nbsp;</td>
              		<td><?php echo $this->Html->link($schedulesdetail['Location']['name'], array('controller' => 'locations', 'action' => 'view', $schedulesdetail['Location']['id']));  ?>&nbsp;</td>
					<td><?php echo $schedulesdetail['Schedulesdetail']['airport']?></td>
					<td><?php echo $schedulesdetail['Schedulesdetail']['crew']; ?>&nbsp;</td>
					<td><?php echo $schedulesdetail['Schedulesdetail']['pilot_identification']; ?>&nbsp;</td>
					<td><?php echo $schedulesdetail['Schedulesdetail']['flight_state']; ?>&nbsp;</td>
					<td><?php echo $schedulesdetail['Schedulesdetail']['pickup_crew'];?>&nbsp;</td>
					<td><?php echo $schedulesdetail['Schedulesdetail']['drop_crew']; ?>&nbsp;</td>
					<td><?php echo $this->Html->link($schedulesdetail['Driver']['name'], array('controller' => 'drivers', 'action' => 'view', $schedulesdetail['Driver']['id']));?>&nbsp;</td>
					<td><?php echo $this->Html->link($schedulesdetail['Vehicle']['model'], array('controller' => 'vehicles', 'action' => 'view', $schedulesdetail['Vehicle']['id'])); ?>&nbsp;</td>					
					<td><?php echo $schedulesdetail['Schedulesdetail']['comments']; ?>&nbsp;</td>
					<td><?php echo $schedulesdetail['Schedulesdetail']['estimated_time']; ?>&nbsp;</td>
					<td>
	                    <div class="btn-group"><?php
	                    	echo $this->Html->link('<i class="icon_pencil-edit"></i>', array(
							        'action'=>'edit',
							         $schedulesdetail['Schedulesdetail']['id']
							    ),
							    array(
							        'class'=>'btn btn-primary',
							        'escape'=>false
							    ));

	                    	echo $this->Html->link('<i class="icon_close_alt2"></i>', array(
							        'action'=>'delete',
							        $schedulesdetail['Schedulesdetail']['id']
							    ),
							    array(
							        'class'=>'btn btn-primary',
							        'escape'=>false
							    ));?> 		              		
						</div>
	              	</td>

				</tr>
                            
           </tbody>
        </table>
		
      </section>
  </div>
</div>
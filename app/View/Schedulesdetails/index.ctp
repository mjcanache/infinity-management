<div class="schedulesdetails index">
	<h2><?php echo __('Schedulesdetails'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('schedule_id'); ?></th>
			<th><?php echo $this->Paginator->sort('location_id'); ?></th>
			<th><?php echo $this->Paginator->sort('airport'); ?></th>
			<th><?php echo $this->Paginator->sort('crew'); ?></th>
			<th><?php echo $this->Paginator->sort('flight_state'); ?></th>
			<th><?php echo $this->Paginator->sort('pickup_crew'); ?></th>
			<th><?php echo $this->Paginator->sort('drop_crew'); ?></th>
			<th><?php echo $this->Paginator->sort('driver_id'); ?></th>
			<th><?php echo $this->Paginator->sort('vehicle_id'); ?></th>
			<th><?php echo $this->Paginator->sort('comments'); ?></th>
			<th><?php echo $this->Paginator->sort('estimated_time'); ?></th>
			<th><?php echo $this->Paginator->sort('date'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($schedulesdetails as $schedulesdetail): ?>
	<tr>
		<td><?php echo h($schedulesdetail['Schedulesdetail']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($schedulesdetail['Schedule']['id'], array('controller' => 'schedules', 'action' => 'view', $schedulesdetail['Schedule']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($schedulesdetail['Location']['name'], array('controller' => 'locations', 'action' => 'view', $schedulesdetail['Location']['id'])); ?>
		</td>
		<td><?php echo h($schedulesdetail['Schedulesdetail']['airport']); ?>&nbsp;</td>
		<td><?php echo h($schedulesdetail['Schedulesdetail']['crew']); ?>&nbsp;</td>
		<td><?php echo h($schedulesdetail['Schedulesdetail']['flight_state']); ?>&nbsp;</td>
		<td><?php echo h($schedulesdetail['Schedulesdetail']['pickup_crew']); ?>&nbsp;</td>
		<td><?php echo h($schedulesdetail['Schedulesdetail']['drop_crew']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($schedulesdetail['Driver']['name'], array('controller' => 'drivers', 'action' => 'view', $schedulesdetail['Driver']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($schedulesdetail['Vehicle']['id'], array('controller' => 'vehicles', 'action' => 'view', $schedulesdetail['Vehicle']['id'])); ?>
		</td>
		<td><?php echo h($schedulesdetail['Schedulesdetail']['comments']); ?>&nbsp;</td>
		<td><?php echo h($schedulesdetail['Schedulesdetail']['estimated_time']); ?>&nbsp;</td>
		<td><?php echo h($schedulesdetail['Schedulesdetail']['date']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $schedulesdetail['Schedulesdetail']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $schedulesdetail['Schedulesdetail']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $schedulesdetail['Schedulesdetail']['id']), array(), __('Are you sure you want to delete # %s?', $schedulesdetail['Schedulesdetail']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Schedulesdetail'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Schedules'), array('controller' => 'schedules', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Schedule'), array('controller' => 'schedules', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Locations'), array('controller' => 'locations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Location'), array('controller' => 'locations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Drivers'), array('controller' => 'drivers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Driver'), array('controller' => 'drivers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Vehicles'), array('controller' => 'vehicles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vehicle'), array('controller' => 'vehicles', 'action' => 'add')); ?> </li>
	</ul>
</div>

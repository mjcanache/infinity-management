              <div class="row">
                <div class="col-lg-12">
                  <h3 class="page-header"><i class="fa fa-file-text-o"></i> Add New row schedule</h3>
                </div>
              </div>
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                        <header class="panel-heading">
                          Form Elements
                        </header>
                        <div class="panel-body"><?php
                          echo $this->Form->create('Schedulesdetail', array(
                                            'url' => array('controller' => 'schedulesdetails', 'action' => 'add'),
                                            'class' => 'form-horizontal',
                                            'label' => false ));
                            echo   $this->Form->input('schedule_id', array('type'=>'hidden', 'value' => $id))?>


                         <div class="form-group">
                              <label class="col-sm-2 control-label">Hotel</label>
                                  <div class="col-sm-10">
                                      <?= $this->Form->input('location_id', array('class' => 'form-control', 'label' => false)) ?>
                                 </div>
                          </div>

                          <div class="form-group">
                              <label class="col-sm-2 control-label">Flight state</label>
                                  <div class="col-sm-10">
                                      <?= $this->Form->input('flight_state', array('class' => 'form-control', 'label' => false, 'options' => Configure::read('INIFINITY.FLIGHT_STATE'))) ?>
                                 </div>
                          </div>

                          <div class="form-group">
                              <label class="col-sm-2 control-label">Crew</label>
                                  <div class="col-sm-10">
                                      <?= $this->Form->input('crew', array('class' => 'form-control', 'label' => false, 'min' =>'1')) ?>
                                 </div>
                          </div>

                          <div class="form-group">
                              <label class="col-sm-2 control-label">Pickup crew</label>
                                  <div class="col-sm-10">
                                      <?= $this->Form->input('pickup_crew', array('class' => 'form-control', 'label' => false, 'timeFormat' => '24')) ?>
                                 </div>
                          </div>

                          <div class="form-group">
                              <label class="col-sm-2 control-label">Drop crew</label>
                                  <div class="col-sm-10">
                                      <?= $this->Form->input('drop_crew', array('class' => 'form-control', 'label' => false, 'timeFormat' => '24')) ?>
                                 </div>
                          </div>                          

                          <div class="form-group">
                              <label class="col-sm-2 control-label">Driver</label>
                                  <div class="col-sm-10">
                                      <?= $this->Form->input('driver_id', array('class' => 'form-control', 'label' => false)) ?>
                                 </div>
                          </div>

                          <div class="form-group">
                              <label class="col-sm-2 control-label">Vehicle</label>
                                  <div class="col-sm-10">
                                      <?= $this->Form->input('vehicle_id', array('class' => 'form-control', 'label' => false)) ?>
                                 </div>
                          </div>

                          <div class="form-group">
                              <label class="col-sm-2 control-label">Comment</label>
                                  <div class="col-sm-10">
                                      <?= $this->Form->input('comments', array('class' => 'form-control', 'label' => false)) ?>
                                 </div>
                          </div>


                          <div class="form-group">
                              <label class="col-sm-2 control-label">Estimated time</label>
                                  <div class="col-sm-10">
                                      <?= $this->Form->input('estimated_time', array('class' => 'form-control', 'label' => false, 'timeFormat' => '24')) ?>
                                 </div>
                          </div>

                           <div class="form-group">
                              <label class="col-sm-2 control-label">Date</label>
                                  <div class="col-sm-10" id="">
                                      <?= $this->Form->input('date', array('class' => 'form-control', 'label' => false, 'id'=>'sandbox-container-solo', 'type'=>'text')) ?>
                                 </div>
                          </div>

                          <button type="submit" class="btn btn-primary" style="float:right;">Submit</button>
                          <?php echo $this->Form->end();?>
                                
                      </section>
                  </div>
              </div>
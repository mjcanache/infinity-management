<div class="row" style="margin-left: 20px">
	<div class="col-lg-12">
	  <h3 class="page-header" style="color:#1A2732; font-family: sans-serif;">Schedule</h3>
	</div>
</div>


<div class="row" style="margin-left: 20px">
  <div class="col-lg-12">
      <section class="panel">      
          <table style="font-family: sans-serif; font-size: 12px; margin-bottom:20px;">
           <tbody>
              <tr>
              	<th style="background-color: #286090; color: white;font-weight: normal; padding-top: 10px; padding-left: 10px; padding-right: 10px; text-align: center;"><?php echo 'Sch type'; ?></th>
              	<th style="background-color: #286090; color: white;font-weight: normal; padding-top: 10px; padding-left: 10px; padding-right: 10px; text-align: center;"><?php echo 'Airline';?></th>
				<th style="background-color: #286090; color: white;font-weight: normal; padding-top: 10px; padding-left: 0px; padding-right: 0px; text-align: center;"><?php echo 'Flight number';?></th>
				<th style="background-color: #286090; color: white;font-weight: normal; padding-top: 10px; padding-left: 10px; padding-right: 10px; text-align: center;"><?php echo 'Flight type';?></th>
				<th style="background-color: #286090; color: white;font-weight: normal; padding-top: 10px; padding-left: 10px; padding-right: 10px; text-align: center;"><?php echo 'Rate (USD)';?></th>
              </tr>

              <?php          	
              	$flight_type = null;
              	if($schedule['Schedule']['flight_type'] == 'Arrived'){
              		$flight_type = '<span class="label label-success">'.$schedule['Schedule']['flight_type'].'</span>';
              	}
              	else{
              		$flight_type = '<span class="label label-danger">'.$schedule['Schedule']['flight_type'].'</span>';
              	}?>
              	<tr>
              		<td style="padding-top: 10px; padding-left: 20px;"><?php echo h($schedule['Schedule']['sch_type']); ?>&nbsp;</td>
					<td style="padding-top: 10px; padding-left: 20px;">
						<?php echo $this->Html->link($schedule['Airline']['code'], array('controller' => 'airlines', 'action' => 'view', $schedule['Airline']['id'])); ?>
					</td>
					<td style="padding-top: 10px; padding-left: 20px;"><?php echo h($schedule['Schedule']['flight_number']); ?>&nbsp;</td>
					<td style="padding-top: 10px; padding-left: 20px;"><?php echo $flight_type;?></td>
					<td style="padding-top: 10px; padding-left: 20px;"><?php echo h($schedule['Schedule']['rate']); ?>&nbsp;</td>
				</tr>
                            
           </tbody>
        </table>
		
      </section>
  </div>
</div>


<div class="row" style="margin-left: 20px">
  <div class="col-lg-12"> 
      <section class="panel">
          <table style="font-family: sans-serif; font-size: 12px;" cellpadding="8">
           <tbody>
              <tr>
              	<th style="background-color: #286090; color: white;font-weight: normal; padding-top: 10px; padding-left: 10px; padding-right: 10px; text-align: center;"><?php echo __('Date'); ?></th>
				<th style="background-color: #286090; color: white;font-weight: normal; padding-top: 10px; padding-left: 10px; padding-right: 10px; text-align: center;"><?php echo __('Hotel'); ?></th>
				<th style="background-color: #286090; color: white;font-weight: normal; padding-top: 10px; padding-left: 10px; padding-right: 10px; text-align: center;"><?php echo __('Airport'); ?></th>
				<th style="background-color: #286090; color: white;font-weight: normal; padding-top: 10px; padding-left: 10px; padding-right: 10px; text-align: center;"><?php echo __('Crew'); ?></th>
				<th style="background-color: #286090; color: white;font-weight: normal; padding-top: 10px; padding-left: 10px; padding-right: 10px; text-align: center;"><?php echo __('Pilot ID');?></th>
				<th style="background-color: #286090; color: white;font-weight: normal; padding-top: 10px; padding-left: 10px; padding-right: 10px; text-align: center;"><?php echo __('Flight State'); ?></th>
				<th style="background-color: #286090; color: white;font-weight: normal; padding-top: 10px; padding-left: 10px; padding-right: 10px; text-align: center;"><?php echo __('Pickup Crew'); ?></th>
				<th style="background-color: #286090; color: white;font-weight: normal; padding-top: 10px; padding-left: 10px; padding-right: 10px; text-align: center;"><?php echo __('Drop Crew'); ?></th>
				<th style="background-color: #286090; color: white;font-weight: normal; padding-top: 10px; padding-left: 10px; padding-right: 10px; text-align: center;"><?php echo __('Driver'); ?></th>
				<th style="background-color: #286090; color: white;font-weight: normal; padding-top: 10px; padding-left: 10px; padding-right: 10px; text-align: center;"><?php echo __('Vehicle'); ?></th>
				<th style="background-color: #286090; color: white;font-weight: normal; padding-top: 10px; padding-left: 10px; padding-right: 10px; text-align: center;"><?php echo __('Estimated Time'); ?></th>
				<th style="background-color:#286090; color: white;font-weight: normal; padding-top: 10px; padding-left: 10px; padding-right: 10px; text-align: center;"><?php echo __('Comments'); ?></th>
              </tr>

              <?php foreach ($schedule['Schedulesdetail'] as $schedulesdetail):?>
              	<tr style="page-break-after: always;">
              		<td style="padding-top: 10px; padding-left: 20px; line-height: 2.42857;"><?php echo $schedulesdetail['date']; ?></td> 
					<td style="padding-top: 10px; padding-left: 20px; line-height: 2.42857;"><?php echo $schedulesdetail['location_id']; ?></td>
					<td style="padding-top: 10px; padding-left: 20px; line-height: 2.42857;"><?php echo $schedulesdetail['airport']; ?></td>
					<td style="padding-top: 10px; padding-left: 20px; line-height: 2.42857;"><?php echo $schedulesdetail['crew']; ?></td>
					<td style="padding-top: 10px; padding-left: 20px; line-height: 2.42857;"><?php echo $schedulesdetail['pilot_identification']; ?>&nbsp;</td>
					<td style="padding-top: 10px; padding-left: 20px; line-height: 2.42857;"><?php echo $schedulesdetail['flight_state']; ?></td>
					<td style="padding-top: 10px; padding-left: 20px; line-height: 2.42857;"><?php echo $schedulesdetail['pickup_crew']; ?></td>
					<td style="padding-top: 10px; padding-left: 20px; line-height: 2.42857;"><?php echo $schedulesdetail['drop_crew']; ?></td>
					<td style="padding-top: 10px; padding-left: 20px; line-height: 2.42857;"><?php echo $schedulesdetail['driver_id']; ?></td>
					<td style="padding-top: 10px; padding-left: 20px; line-height: 2.42857;"><?php echo $schedulesdetail['vehicle_id']; ?></td>
					<td style="padding-top: 10px; padding-left: 20px; line-height: 2.42857;"><?php echo $schedulesdetail['estimated_time']; ?></td>
					<td style="padding-top: 10px; padding-left: 20px; line-height: 2.42857;"><?php echo $schedulesdetail['comments']; ?></td>
				</tr>
			<?php endforeach; ?>
                            
           </tbody>
        </table>
		
      </section>
  </div>
</div>
<div class="row">
	<div class="col-lg-6">
	  <h3 class="page-header"><i class="fa fa-file-text-o"></i> View schedule</h3>
	</div>
	<div class="col-lg-6"><?php 
		echo $this->Html->link('<i class="fa fa-share"></i> Export to PDF', array('action'=>'view', $schedule['Schedule']['id'],'ext' => 'pdf'),  array('escape'=>false,'style' => 'float:right'));?> 
	</div>
</div>


<div class="row">
  <div class="col-lg-12">
      <section class="panel">      
          <table class="table table-striped table-advance table-hover">
           <tbody>
              <tr>
              	<th><?php echo 'Sch type'; ?></th>
              	<th><?php echo 'Airline';?></th>
				<th><?php echo 'Flight number';?></th>
				<th><?php echo 'Flight type';?></th>
				<th><?php echo 'Rate (USD)';?></th>
				<th><?php echo 'created';?></th>
				<th><?php echo 'updated';?></th>
				<th class="actions"><?php echo __('Actions'); ?></th>    
              </tr>

              <?php          	
              	$flight_type = null;
              	if($schedule['Schedule']['flight_type'] == 'Arrived'){
              		$flight_type = '<span class="label label-success">'.$schedule['Schedule']['flight_type'].'</span>';
              	}
              	else{
              		$flight_type = '<span class="label label-danger">'.$schedule['Schedule']['flight_type'].'</span>';
              	}?>
              	<tr>
              		<td><?php echo h($schedule['Schedule']['sch_type']); ?>&nbsp;</td>
					<td>
						<?php echo $this->Html->link($schedule['Airline']['code'], array('controller' => 'airlines', 'action' => 'view', $schedule['Airline']['id'])); ?>
					</td>
					<td><?php echo h($schedule['Schedule']['flight_number']); ?>&nbsp;</td>
					<td><?php echo $flight_type;?></td>
					<td><?php echo h($schedule['Schedule']['rate']); ?>&nbsp;</td>
					<td><?php echo date("d-m-Y", strtotime($schedule['Schedule']['created'])); ?>&nbsp;</td>
					<td><?php echo date("d-m-Y", strtotime($schedule['Schedule']['updated']));?>&nbsp;</td>
					<td>
	                    <div class="btn-group"><?php

	                    	echo $this->Html->link('<i class="icon_pencil-edit"></i>', array(
							        'action'=>'edit',
							        $schedule['Schedule']['id']
							    ),
							    array(
							        'class'=>'btn btn-primary',
							        'escape'=>false
							    ));

	                    	echo $this->Html->link('<i class="icon_close_alt2"></i>', array(
							        'action'=>'delete',
							        $schedule['Schedule']['id']
							    ),
							    array(
							        'class'=>'btn btn-primary',
							        'escape'=>false
							    ));?> 		              		
						</div>
	              	</td>
				</tr>
                            
           </tbody>
        </table>
		
      </section>
  </div>
</div>


<div class="row">
  <div class="col-lg-12"><?php
        	echo $this->Html->link('<span class="icon_plus_alt2"></span> Add new', array(
			'controller' => 'schedulesdetails',
	        'action'=>'add',
	        $schedule['Schedule']['id']
		    ),
		    array(
		        'class'=>'btn btn-success btn-sm',
		        'style' => 'margin-bottom: 10px;',
		        'escape'=>false
		    ));?>   
      <section class="panel">
          <table class="table table-striped table-advance table-hover">
           <tbody>
              <tr>
              	<th><?php echo __('Date'); ?></th>
				<th><?php echo __('Hotel'); ?></th>
				<th><?php echo __('Airport'); ?></th>
				<th><?php echo __('Crew'); ?></th>
				<th><?php echo __('Pilot ID');?></th>
				<th><?php echo __('Flight State'); ?></th>
				<th><?php echo __('Pickup Crew'); ?></th>
				<th><?php echo __('Drop Crew'); ?></th>
				<th><?php echo __('Driver'); ?></th>
				<th><?php echo __('Vehicle'); ?></th>
				<th><?php echo __('Estimated Time'); ?></th>
				<th><?php echo __('Comments'); ?></th>
				<th class="actions"><?php echo __('Actions'); ?></th>    
              </tr>

              <?php foreach ($schedule['Schedulesdetail'] as $schedulesdetail):?>
              	<tr>
              		<td><?php echo $schedulesdetail['date']; ?></td> 
					<td><?php echo $schedulesdetail['location_id']; ?></td>
					<td><?php echo $schedulesdetail['airport']; ?></td>
					<td><?php echo $schedulesdetail['crew']; ?></td>
					<td><?php echo $schedulesdetail['pilot_identification']; ?>&nbsp;</td>
					<td><?php echo $schedulesdetail['flight_state']; ?></td>
					<td><?php echo $schedulesdetail['pickup_crew']; ?></td>
					<td><?php echo $schedulesdetail['drop_crew']; ?></td>
					<td><?php echo $schedulesdetail['driver_id']; ?></td>
					<td><?php echo $schedulesdetail['vehicle_id']; ?></td>
					<td><?php echo $schedulesdetail['estimated_time']; ?></td>
					<td><?php echo $schedulesdetail['comments']; ?></td>
					<td>
		                <div class="btn-group"><?php 
		                	echo $this->Html->link('<i class="icon_check"></i>', array(
		                			'controller' => 'schedulesdetails',
							        'action'=>'view',
							        $schedulesdetail['id']
							    ),
							    array(
							        'class'=>'btn btn-primary',
							        'escape'=>false
							    ));

		                	echo $this->Html->link('<i class="icon_pencil-edit"></i>', array(
							        'controller' => 'schedulesdetails',
							        'action'=>'edit',
							        $schedulesdetail['id']
							    ),
							    array(
							        'class'=>'btn btn-primary',
							        'escape'=>false
							    ));

		                	echo $this->Html->link('<i class="icon_close_alt2"></i>', array(
							        'controller' => 'schedulesdetails',
							        'action'=>'delete',
							        $schedulesdetail['id'], $schedule['Schedule']['id']
							    ),
							    array(
							        'class'=>'btn btn-primary',
							        'escape'=>false
							    ));?> 		              		
						</div>
		          	</td>
				</tr>
			<?php endforeach; ?>
                            
           </tbody>
        </table>
		
      </section>
  </div>
</div>




<div class="schedules form">
<?php echo $this->Form->create('Schedule'); ?>
	<fieldset>
		<legend><?php echo __('Add Schedule'); ?></legend>
	<?php
		echo $this->Form->input('flight_number');
		echo $this->Form->input('airline_id');
		echo $this->Form->input('flight_type');
		echo $this->Form->input('rate');
		echo $this->Form->input('sch_type');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Schedules'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Airlines'), array('controller' => 'airlines', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Airline'), array('controller' => 'airlines', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Schedulesdetails'), array('controller' => 'schedulesdetails', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Schedulesdetail'), array('controller' => 'schedulesdetails', 'action' => 'add')); ?> </li>
	</ul>
</div>

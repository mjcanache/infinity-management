<div class="row">
	<div class="col-lg-12">
	  <h3 class="page-header"><i class="fa fa-file-text-o"></i> Schedules</h3>
	</div>
</div>

<div class="row">
  <div class="col-lg-12">
      <section class="panel">
          <header class="panel-heading">
              Search by flight number and flight type 
          </header>
          <div class="panel-body"><?php
                echo $this->Form->create('Schedule', array(
                                    'url' => array('controller' => 'schedules', 'action' => 'index'),
                                    'class' => 'form-inline',
                                    'label' => false ));?>
                      <!-- Para una sola fecha del calendario: sandbox-container-solo -->
                      <div class="form-group">
                              <div class="col-sm-10">
                                  <?= $this->Form->input('flight_number', array('class' => 'form-control', 'label' => false, 'placeholder' => 'Flight number')) ?>
                              </div>
                          </div>

                      <div class="form-group">
                          <label class="sr-only">Filter</label>                             
                          <?= $this->Form->input('flight_type', array('class' => 'form-control', 'label' => false, 'options' => Configure::read('INIFINITY.FLIGHT_TYPE_SEARCH'))) ?>                       
                      </div>

	                  <button type="submit" class="btn btn-primary">search</button>
               <?php echo $this->Form->end();?>
          </div>
      </section>

  </div>
</div>

<div class="row">
  <div class="col-lg-12">
      <section class="panel">      
          <table class="table table-striped table-advance table-hover">
           <tbody>
              <tr>
              	<th><?php echo $this->Paginator->sort('sch_type'); ?></th>
              	<th><?php echo $this->Paginator->sort('airline_id'); ?></th>
				<th><?php echo $this->Paginator->sort('flight_number'); ?></th>
				<th><?php echo $this->Paginator->sort('flight_type'); ?></th>
				<th><?php echo $this->Paginator->sort('rate').' (USD)';?></th>
				<th><?php echo $this->Paginator->sort('created'); ?></th>
				<th><?php echo $this->Paginator->sort('updated'); ?></th>
				<th class="actions"><?php echo __('Actions'); ?></th>    
              </tr>

              <?php foreach ($schedules as $schedule): 
              	$flight_type = null;
              	if($schedule['Schedule']['flight_type'] == 'Arrived'){
              		$flight_type = '<span class="label label-success">'.$schedule['Schedule']['flight_type'].'</span>';
              	}
              	else{
              		$flight_type = '<span class="label label-danger">'.$schedule['Schedule']['flight_type'].'</span>';
              	}
              ?>
              	<tr>
              		<td><?php echo h($schedule['Schedule']['sch_type']); ?>&nbsp;</td>
					<td>
						<?php echo $this->Html->link($schedule['Airline']['code'], array('controller' => 'airlines', 'action' => 'view', $schedule['Airline']['id'])); ?>
					</td>
					<td><?php echo h($schedule['Schedule']['flight_number']); ?>&nbsp;</td>
					<td><?php echo $flight_type;?></td>
					<td><?php echo h($schedule['Schedule']['rate']); ?>&nbsp;</td>
					<td><?php echo date("d-m-Y", strtotime($schedule['Schedule']['created']));?>&nbsp;</td>
					<td><?php echo date("d-m-Y", strtotime($schedule['Schedule']['updated']));?></td>
					<td>
	                    <div class="btn-group"><?php 
	                    	echo $this->Html->link('<i class="icon_check"></i>', array(
							        'action'=>'view',
							        $schedule['Schedule']['id']
							    ),
							    array(
							        'class'=>'btn btn-primary',
							        'escape'=>false
							    ));

	                    	echo $this->Html->link('<i class="icon_pencil-edit"></i>', array(
							        'action'=>'edit',
							        $schedule['Schedule']['id']
							    ),
							    array(
							        'class'=>'btn btn-primary',
							        'escape'=>false
							    ));

	                    	echo $this->Html->link('<i class="icon_close_alt2"></i>', array(
							        'action'=>'delete',
							        $schedule['Schedule']['id']
							    ),
							    array(
							        'class'=>'btn btn-primary',
							        'escape'=>false
							    ));?> 		              		
						</div>
	              	</td>
				</tr>
			<?php endforeach; ?>
                            
           </tbody>
        </table>
		
      </section>
  </div>
</div>

<div class="row">
	<div class="col-lg-12" style="text-align:right;">
		<p class="breadcrumb"><?php
			echo $this->Paginator->counter(array(
			'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
			));
			?>	</p>
			<div class="paging">
			<?php
				echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled')).' ';
				echo $this->Paginator->numbers(array('separator' => ' '));
				echo $this->Paginator->next(__(' next') . ' >', array(), null, array('class' => 'next disabled'));
			?>
			</div>
	</div>
</div>
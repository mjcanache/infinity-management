              <div class="row">
                <div class="col-lg-12">
                  <?php if($user['is_admin']){?>
                    <h3 class="page-header"><i class="fa fa-file-text-o"></i> New Schedule</h3>
                  <?php }else{?>
                    <h3 class="page-header"><i class="fa fa-file-text-o"></i> Add New Schedule - Dispatch only</h3>
                  <?php } ?>
                </div>
              </div>
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                        <header class="panel-heading">
                          Form Elements
                        </header>
                        <div class="panel-body"><?php  
                          echo $this->Form->create('Schedule', array(
                                            'url' => array('controller' => 'schedules', 'action' => 'addnew'),
                                            'class' => 'form-horizontal',
                                            'label' => false ))?>

                          <div class="form-group">
                              <label class="col-sm-2 control-label">Flight number</label>
                              <div class="col-sm-10">
                                  <?= $this->Form->input('flight_number', array('class' => 'form-control', 'label' => false)) ?>
                              </div>
                          </div>

                          <div class="form-group">
                              <label class="col-sm-2 control-label">Airline</label>
                                  <div class="col-sm-10">
                                      <?= $this->Form->input('airline_id', array('class' => 'form-control', 'label' => false)) ?>
                                 </div>
                          </div>

                          <?php if($user['is_admin']){?>
                           <div class="form-group">
                              <label class="col-sm-2 control-label">Flight type</label>
                                  <div class="col-sm-10">
                                      <?= $this->Form->input('flight_type', array('class' => 'form-control', 'label' => false, 'options' => Configure::read('INIFINITY.FLIGHT_TYPE'))) ?>
                                 </div>
                          </div>                         

                          <div class="form-group">
                              <label class="col-sm-2 control-label">Hotel</label>
                                  <div class="col-sm-10">
                                      <?= $this->Form->input('location_id', array('class' => 'form-control', 'label' => false)) ?>
                                 </div>
                          </div>
                          <?php }else{?>
                          <div class="form-group">
                              <label class="col-sm-2 control-label">Pickup at</label>
                                  <div class="col-sm-10">
                                      <?= $this->Form->input('Schedulesadd.pickup_at', array('class' => 'form-control', 'label' => false)) ?>
                                 </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 control-label">Drop at</label>
                                  <div class="col-sm-10">
                                      <?= $this->Form->input('Schedulesadd.drop_at', array('class' => 'form-control', 'label' => false)) ?>
                                 </div>
                          </div>
                          <?php }?>

                          <div class="form-group">
                              <label class="col-sm-2 control-label">Crew</label>
                                  <div class="col-sm-10">
                                      <?= $this->Form->input('crew', array('class' => 'form-control', 'label' => false, 'min' =>'1')) ?>
                                 </div>
                          </div>

                          <div class="form-group">
                              <label class="col-sm-2 control-label">Estimated time</label>
                                  <div class="col-sm-10">
                                      <?= $this->Form->input('estimated_time', array('class' => 'form-control', 'label' => false, 'timeFormat' => '24')) ?>
                                 </div>
                          </div>

                          <?php if($user['is_admin']){?>
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Dates</label>
                                  <div class="col-sm-10" id="">
                                      <?= $this->Form->input('date', array('class' => 'form-control', 'label' => false, 'id'=>'sandbox-container')) ?>
                                 </div>
                            </div>
                          <?php } else{?>
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Dates</label>
                                  <div class="col-sm-10" id="">
                                      <?= $this->Form->input('date', array('class' => 'form-control', 'label' => false, 'id'=>'sandbox-container-solo')) ?>
                                 </div>
                            </div>
                          <?php } ?>

                          <button type="submit" class="btn btn-primary" style="float:right;">Submit</button>
                          <?php echo $this->Form->end();?>
                                
                      </section>
                  </div>
              </div>
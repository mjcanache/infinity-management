<div class="row">
	<div class="col-lg-12">
	 	<h3 class="page-header"><i class="fa fa-file-text-o"></i> Edit current schedule</h3>
	</div>
</div>

<div class="row">
  <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading">
          Form Elements
        </header>
        <div class="panel-body"><?php
          echo $this->Form->create('Schedule', array(
          					'url' => array('controller' => 'schedules', 'action' => 'edit'),
                            'class' => 'form-horizontal',
                            'label' => false )); 
          echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$id)); ?>


          <div class="form-group">
              <label class="col-sm-2 control-label">Flight Number</label>
                  <div class="col-sm-10">
                      <?= $this->Form->input('flight_number', array('class' => 'form-control', 'label' => false)) ?>
                 </div>
          </div> 

          <div class="form-group">
              <label class="col-sm-2 control-label">Airline</label>
                  <div class="col-sm-10">
                      <?= $this->Form->input('airline_id', array('class' => 'form-control', 'label' => false)) ?>
                 </div>
          </div>                 

          <div class="form-group">
              <label class="col-sm-2 control-label">Flight type</label>
                  <div class="col-sm-10">
                      <?= $this->Form->input('flight_type', array('class' => 'form-control', 'label' => false, 'options' => Configure::read('INIFINITY.FLIGHT_TYPE'))) ?>
                 </div>
          </div>


          <div class="form-group">
              <label class="col-sm-2 control-label">Rate</label>
                  <div class="col-sm-10">
                      <?= $this->Form->input('rate', array('class' => 'form-control', 'label' => false)) ?>
                 </div>
          </div>
    

          <button type="submit" class="btn btn-primary" style="float:right;">Submit</button>
          <?php echo $this->Form->end();?>
                
      </section>
  </div>
</div>
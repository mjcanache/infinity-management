<?php
App::uses('AppModel', 'Model');
App::uses('Component', 'Model');
/**
 * Tienda Model
 *
 * @property Tienda $Tienda
 */
class Schedule extends AppModel {

	public $useTable = 'schedules';


/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'flight_number' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'naturalNumber' => array(
				'rule' => array('naturalNumber'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'airline_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'flight_type' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'rate' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'money' => array(
				'rule' => array('money'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);


/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Airline' => array(
			'className' => 'Airline',
			'foreignKey' => 'airline_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public $hasMany = array(
		'Schedulesdetail' => array(
			'className' => 'Schedulesdetail',
			'foreignKey' => 'schedule_id',
			'dependent' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => 'false',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

function putDataTogether($data_form, $sch_type = 'SCH'){
	
      	$loops = explode(',', $data_form['Schedule']['date']);
      	$is_Schedulesadd = isset($data_form['Schedulesadd']);
      	$Schedulesadd = ClassRegistry::init('Airline');
      	$rate = $this->Airline->findById($data_form['Schedule']['airline_id']);
 		
      	if($is_Schedulesadd){
	      	$ds_schedule =  array(
				              'flight_number' => $data_form['Schedule']['flight_number'],
				              'airline_id' => $data_form['Schedule']['airline_id'],
				              'sch_type' => $sch_type,
				              'rate' => $rate['Airline']['rate']);  

		    $ds_scheduledetail[] = array(								       
							        'crew' => $data_form['Schedule']['crew'],
							        'estimated_time' => $data_form['Schedule']['estimated_time'],
							        'driver_id' => 1,
							        'vehicle_id' => 1,
							        'date' => date("Y-m-d", strtotime($data_form['Schedule']['date'])));   		

    	}else{
	      	$ds_schedule =  array(
                  'flight_number' => $data_form['Schedule']['flight_number'],
                  'airline_id' => $data_form['Schedule']['airline_id'],
                  'flight_type' => $data_form['Schedule']['flight_type'],
                  'sch_type' => $sch_type,
                  'rate' => $rate['Airline']['rate']);  

    		foreach ($loops as $loop)
		    {
		      $ds_scheduledetail[] = array(
								        'location_id' => $data_form['Schedule']['location_id'],
								        'crew' => $data_form['Schedule']['crew'],
								        'estimated_time' => $data_form['Schedule']['estimated_time'],
								        'driver_id' => 1,
								        'vehicle_id' => 1,
								        'date' => date("Y-m-d", strtotime($loop)));

		    }
    	}

     	$schedules['Schedule'] = $ds_schedule;
     	$schedules['Schedulesdetail'] = $ds_scheduledetail;

    	$save = $this->saveAssociated($schedules, array('deep' => true));

    	if($save && $is_Schedulesadd){
    		$ds_schedulesadds = array(
    								'schedulesdetail_id' => $this->Schedulesdetail->id,
    								'pickup_at' => $data_form['Schedulesadd']['pickup_at'],
    								'drop_at' => $data_form['Schedulesadd']['drop_at'],
    								);

			$Schedulesadd = ClassRegistry::init('Schedulesadd');
    		$save = $Schedulesadd->save($ds_schedulesadds);
    	}

    	return $save;
	}
}
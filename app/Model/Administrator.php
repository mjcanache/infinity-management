<?php
App::uses('AppModel', 'Model');

/**
 * Usuario Model
 */
class Administrator extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'administrator';

	/**
	 * Display field
	 *
	 * @var string
	 */
	public $displayField = 'id';


	public function beforeSave($options = array())
	{
		if (isset($this->data[$this->alias]['pass']))
		{
			$this->data[$this->alias]['pass'] = AuthComponent::password($this->data[$this->alias]['pass']);
		}

		if (isset($this->data[$this->alias]['login']))
		{
			$this->data[$this->alias]['login'] = strtolower($this->data[$this->alias]['login']);
		}

		/*if (isset($this->data[$this->alias]['email']))
		{
			$this->data[$this->alias]['email'] = strtolower($this->data[$this->alias]['email']);
		}*/

		return true;
	}


	/**
	 * Esta funcion debe ser llamada para setear y encriptar el password de un usuario ya creado en base de datos,
	 * con el objetivo de que pueda hacer login.
	 *
	 * Lo demás se debe hacer manualmente.
	 *
	 * No olvides colocar el id del usuario correcto y establecer el password a tu gusto.
	 */
	public function setpassword()
	{
		// Comentar este return para poder usar la funcion y comentarlo una vez terminado el uso.
		return ;

		$usuario = array(
				'id' => 1,
				'password' => 'lnxkdjfghsuhtr'
		);

		$this->save($usuario);

		$usuario = array(
				'id' => 2,
				'password' => '9476nvfhsdhgj'
		);

		$this->save($usuario);

		$usuario = array(
				'id' => 3,
				'password' => 'isihtulshuhfvs'
		);

		$this->save($usuario);

	}
	public function validate_passwords() {
		return $this->data[$this->alias]['pass'] === $this->data[$this->alias]['pass2'];
	}


	function getParaNotificacion($id)
	{
		//return $this->findById($id);
		$conditions = array("Usuario.id" => $id);

		$valor = $this->find('first', array('conditions' => $conditions,  'recursive' => 1));
		//$this->log($valor);
		return $valor;
	}

}

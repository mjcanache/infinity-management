<?php
App::uses('AppModel', 'Model');
/**
 * Schedulesadd Model
 *
 * @property Schedulesdetail $Schedulesdetail
 */
class Schedulesadd extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Schedulesdetail' => array(
			'className' => 'Schedulesdetail',
			'foreignKey' => 'schedulesdetail_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}